
const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({


productName: {

		type:String,
		required: [ true,  "Please add product name"]

},

productPrice: {

		type:String,
		required: [true, "Please fill up your email"]


},


// FOR MULTER ROUTE ONLY

ProfilePicture: {

		type:String,
		required: [true, "description needed"]
},



productDescription: {

	type:String,
	required: [true, "description needed"]
},



isActive : {

	type: Boolean,
	default:true
},


ordered: [
		{
			userId: {
				type: String,
				required: [true, 'product ID is required']
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			},
			
			status: {
				type: String,
				default: "Order is being processed"
			}
		}
	]
})




module.exports = mongoose.model('Product', productSchema);	