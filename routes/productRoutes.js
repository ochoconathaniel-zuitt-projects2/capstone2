

const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');
const upload = require('../upload.js')


// CREATE PRODUCT ROUTES

router.post('/createItem', auth.verify, (req,res) => {

	const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	
	}

	if(data.isAdmin){
		
		productController.createItem(req.body).then(result => res.send(result))
	
	}else{
		
		return ('you are not an admin !');
	}
})






// GET ALL PRODUCTS


router.get('/get/products', (req, res) =>{
	productController.getAllProducts().then(result => res.send(result))
})







// GET ALL ACTIVE PRODUCTS

router.get('/active', (req, res) =>{
	productController.getAllActiveProducts().then(result => res.send(result))
})




// GET SPECIFIC PRODUCT

router.get('/:productId', (req, res) =>{
	productController.getSpecific(req.params).then(result => res.send(result))
})




// UPDATE A PRODUCT

router.put('/:productId/', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.updateItem(req.params,req.body).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}

})




// ARCHIVE A PRODUCT

router.put('/:productId/archive', auth.verify, (req,res) => {



	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.archiveProduct(req.params).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}


})





// DELETE A PRODUCT

router.delete('/:productId/removeItem', auth.verify , (req,res) => {

		const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){

		productController.deleteItem(req.params).then(result => res.send(result))
	} 
	else {

		return ('you are not an admin !');
	}


})





// asdasd


router.get( '/', (req, res) =>{
	productController.Tops().then(result => res.send(result))
})






// pants

router.get( '/product/pants', (req, res) =>{
	productController.Pants().then(result => res.send(result))
})




// shoes

router.get( '/product/shoes', (req, res) =>{
	productController.Shoes().then(result => res.send(result))
})




// accesories

router.get( '/product/accesories', (req, res) =>{
	productController.Accesories().then(result => res.send(result))
})









// try cmulter

router.post('/uploads', upload , (req,res) => {


		
		productController.createFile(req.body,req.file).then(result => res.send(result))
	

})













// activate


router.put('/:productId/activate', auth.verify, (req, res) =>{
	productController.activateProduct(req.params).then(result => res.send(result))
})








module.exports = router;
